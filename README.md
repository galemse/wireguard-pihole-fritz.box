# pihole

install pihole as root like described here: https://docs.pi-hole.net/main/basic-install/

`curl -sSL https://install.pi-hole.net | bash`

will ask you a lot of questions, read properly and choose wisely ;)

to set admin password:
`pihole -a -p`

# wireguard

see 00_some_commands.sh

## configuration
is in this project

# configure fritz box
## pihole

Goto Heimnetzwerk -> Netzwerk -> Netzwerkeinstellungen

Scroll down to:

IP-Adressen
Hier können Sie Anpassungen an Netzwerkeinstellungen und den verwendeten IP-Adressen in Heimnetz und Gastnetz vornehmen.

IPv4-Einstellungen
IPv6-Einstellungen

in this "IPx-Einstellungen" there are the "Lokaler DNS-Server:", put there the IP of your pihole-Server.

## wireguard
Goto Internet -> Freigaben -> "Gerät für Freigaben hinzufügen" 

Use a Port to forward to your wireguard-server.
