# install packages on pi z2
apt install libmnl-dev libelf-dev build-essential git wireguard openresolv tshark -y
# list status of wireguard
wg

# New User
## generate new keys and preshared key for a client
cd /etc/wireguard
$(umask 077 && wg genkey | tee peer1_private.key | wg pubkey > peer1_public.key)
wg genpsk  # to stdout
cp client_template.conf to new_client.conf
vi new_client.conf
cat new_client.conf | qrencode -t ansiutf8

## add new peer to server config
vi /etc/wireguard/wg0.conf
## restart wireguard:
wg-quick down wg0
wg-quick up wg0


# look at packets
tshark -i wg0            # read packets on interface wg0
tshark -i wg0 "port 53"  # dns packets in wireguard

# configurations
vi /etc/sysctl.d/97-wireguard.conf
vi /etc/wireguard/wg0.conf
vi /etc/nftables.conf

# systemcontrols, enable wireguard at starttime
systemctl enable wg-quick@wg0
systemctl start wg-quick@wg0
systemctl status wg-quick@wg0
systemctl restart nftables.service
systemctl status nftables.service

# if you change smth in sysctl configs
sysctl --system
